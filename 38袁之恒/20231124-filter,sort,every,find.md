# filter
filter()：用于筛选数组中的元素，返回满足条件的元素组成的新数组。

```javascript
const numbers = [1, 2, 3, 4];
const evenNumbers = numbers.filter(num => num % 2 === 0);
console.log(evenNumbers); // 输出: [2, 4]
```
# sort()
sort()：用于对数组中的元素进行排序。

```javascript
const fruits = ['apple', 'orange', 'banana', 'grape'];
fruits.sort();
console.log(fruits); // 输出: ['apple', 'banana', 'grape', 'orange']
```
# every()
every()：用于检查数组中的所有元素是否满足指定条件。

```javascript
const numbers = [2, 4, 6, 8];
const allEven = numbers.every(num => num % 2 === 0);
console.log(allEven); // 输出: true
```
# find
find()：用于找出数组中第一个满足条件的元素。

```javascript
const numbers = [5, 10, 15, 20];
const firstMultipleOfFive = numbers.find(num => num % 5 === 0);
console.log(firstMultipleOfFive); // 输出: 5
```
# findindex
findIndex()：用于找出数组中第一个满足条件的元素的索引。

```javascript
const numbers = [5, 10, 15, 20];
const index = numbers.findIndex(num => num % 15 === 0);
console.log(index); // 输出: 2
```
